package com.crestbit.disasterresponseandrecovery.api;


import com.crestbit.disasterresponseandrecovery.model.SensorData;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

/**
 * Created by Krishna on 3/1/2017.
 */


public interface RestApi {

    @Headers( "Content-Type: application/json" )
    @GET("channels/227379/feeds.json?results=2")
    Observable<SensorData> fechLowArea();

    @Headers( "Content-Type: application/json" )
    @GET("channels/228265/feeds.json?results=2")
    Observable<SensorData> fetchDamRelease();

    @Headers( "Content-Type: application/json" )
    @GET("channels/147165/feeds.json?results=2")
    Observable<SensorData> fetchBridgeLevel();

    @Headers( "Content-Type: application/json" )
    @GET("channels/132714/feeds.json?results=2")
    Observable<SensorData> fetchDamLevel();

    @Headers( "Content-Type: application/json" )
    @GET("channels/334254/feeds.json?results=2")
    Observable<SensorData> fetchDestination();


    //swargate ,shivajinagar,deccan,baner,hadpsar,bibwewadi

}
