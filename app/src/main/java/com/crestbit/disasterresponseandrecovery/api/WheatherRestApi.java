package com.crestbit.disasterresponseandrecovery.api;


import com.crestbit.disasterresponseandrecovery.model.Wheather.WheatherData;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

public interface WheatherRestApi {

    @Headers( "Content-Type: application/json" )
    @GET("forecasts/v1/daily/5day/204848?apikey=Ah6ckFMYUfx5PK8Q8EaDtiPqtVGb9tkv&language=en-us&details=true&metric=false")
    Observable<WheatherData> fetchWheather();

    //old==F3xHrOVrYUNQOz5qJrjsASM8FkdBHNqX
    //Ah6ckFMYUfx5PK8Q8EaDtiPqtVGb9tkv
    //XKRAd6UfdnkbhOzKmyenuIjGg6Hf6mXR
}
