package com.crestbit.disasterresponseandrecovery.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crestbit.disasterresponseandrecovery.BuildConfig;
import com.crestbit.disasterresponseandrecovery.R;
import com.crestbit.disasterresponseandrecovery.api.RestApi;
import com.crestbit.disasterresponseandrecovery.model.Feed;
import com.crestbit.disasterresponseandrecovery.model.SensorData;
import com.crestbit.disasterresponseandrecovery.model.Station;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private RestApi restApi;
    public static String URL = "https://api.thingspeak.com/";
    MarkerOptions hospitalMarker1 = new MarkerOptions();
    MarkerOptions ambulanceMarker = new MarkerOptions();

    Marker mMarker;
    ImageView backArrow,optionMenu,addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        backArrow=findViewById(R.id.backButton);
        optionMenu=findViewById(R.id.optionMenu);
        addButton=findViewById(R.id.addButton);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addService();
            }
        });
        optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAmbulanceData();
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void addService() {
        startActivity(new Intent(MapsActivity.this,AddServiceActivity.class));
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    protected void onStart() {
        super.onStart();
        initRestApi();
        getAmbulanceData();
    }

    private void getAmbulanceData() {

        Observable<SensorData> observable= restApi.fetchDestination();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SensorData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","error");
                    }

                    @Override
                    public void onNext(SensorData user) {

                        List<Feed> feeds = user.getFeeds();
                        Feed feed = feeds.get(feeds.size() - 1);
                        Log.d("<<<<<", " "+feed.getField1());
                        String lat=feed.getField1();
                        String lon=feed.getField2();

                        LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                        //18.517083, 73.839769
                        if (!lat.equals("0")&&!lon.equals("0")) {
                            ambulanceMarker.position(latLng);
                            ambulanceMarker.title("Ambulance");
                            ambulanceMarker.snippet("amblLive");
                            ambulanceMarker.flat(false);

                            ambulanceMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance));
                            marker1=mMap.addMarker(ambulanceMarker);
                        }
                        else {
                            Toast.makeText(MapsActivity.this, "Error while getting location", Toast.LENGTH_SHORT).show();
                        }


                    }

                });
    }

    private void initRestApi() {
        OkHttpClient client = new OkHttpClient.Builder()/*.connectTimeout(100, TimeUnit.SECONDS).readTimeout(300, TimeUnit.SECONDS)*/.addInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE)).build();
        restApi =new Retrofit.Builder()
                .client(client)
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(getGsonConverter()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(RestApi.class);
    }

    private Gson getGsonConverter() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        addMarkersOnMap();
        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(18.457716, 73.869808);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        LatLng currentLocation = new LatLng(18.495851, 73.865356);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);

    }
    private static Marker marker1;

    private void addMarkersOnMap() {


        //MarkerOptions [] markerOptions=new MarkerOptions[5];
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Stations");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    MarkerOptions[] markerOptions = new MarkerOptions[(int) dataSnapshot.getChildrenCount()];
                    for (int i = 0; i < markerOptions.length; i++) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            String len= String.valueOf(markerOptions.length);

                            Station station = snapshot.getValue(Station.class);


                            LatLng latLng = new LatLng(Double.parseDouble(station.lat), Double.parseDouble(station.lang));
                               /* marker1 = mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .title(station.title)
                                        .snippet(station.id)
                                        .flat(false)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_large)));*/
                            markerOptions[i] = new MarkerOptions();
                            markerOptions[i].position(latLng);
                            markerOptions[i].title(station.title);
                            markerOptions[i].snippet(station.id);
                            markerOptions[i].flat(false);
                            if (station.title.equals("Hospital")) {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_large));
                            }
                            else if (station.title.equals("Boat"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.ship));
                            }
                            else if (station.title.equals("Fire Station"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.fire));
                            }
                            else if (station.title.equals("Blood Bank"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.blood));
                            }
                            else if (station.title.equals("Transport Vehicle"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.bus));
                            }
                            else if (station.title.equals("Ambulance"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance));
                            }else if (station.title.equals("Food"))
                            {
                                markerOptions[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.diet));
                            }


                            marker1 = mMap.addMarker(markerOptions[i]);

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
       /* LatLng latLng = new LatLng(18.456902, 73.870194);
        hospitalMarker1.position(latLng);
        hospitalMarker1.title("Hospital");
        hospitalMarker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_large));
        hospitalMarker1.flat(false);
        hospitalMarker1.snippet("1");

        LatLng latLng2 = new LatLng(18.457716, 73.869808);
        hospitalMarker2.position(latLng2);
        hospitalMarker2.title("Hospital");
        hospitalMarker2.icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_large));
        hospitalMarker2.flat(false);
        hospitalMarker2.snippet("2");


        marker1 = mMap.addMarker(hospitalMarker1);
        marker1 = mMap.addMarker(hospitalMarker2);*/
        mMap.setOnMarkerClickListener(this);

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        if (marker.getSnippet().equals("amblLive"))
        {
            final Dialog marketDialog = new Dialog(this, R.style.Theme_AppCompat_Light_Dialog);
            marketDialog.setContentView(R.layout.marker_dialog);
            Button closeButton = marketDialog.findViewById(R.id.closeButton);
            final TextView heading=marketDialog.findViewById(R.id.heading);
            final TextView title = marketDialog.findViewById(R.id.title);
            final TextView contact = marketDialog.findViewById(R.id.contact);
            final TextView general = marketDialog.findViewById(R.id.general);
            final TextView icu = marketDialog.findViewById(R.id.icu);
            final TextView avail = marketDialog.findViewById(R.id.avail);
            final ImageView callButton = marketDialog.findViewById(R.id.callButton);
            final ImageView tagImageView = marketDialog.findViewById(R.id.tagImageView);

            heading.setText("Ambulance");
            tagImageView.setBackgroundResource(R.drawable.ambulance);
            title.setText("Live Tracking Ambulance");
            contact.setText("8087445956");
            general.setVisibility(View.GONE);
            icu.setVisibility(View.GONE);
            avail.setVisibility(View.GONE);
            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(MapsActivity.this)
                            .setCancelable(false)
                            .setMessage("Do you want to make contact with Ambulance " +  " ?")
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_CALL);
                                    intent.setData(Uri.parse("tel:" +"8087445956"));
                                    startActivity(intent);
                                }
                            }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder1.create().show();
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    marketDialog.dismiss();
                }
            });

            marketDialog.show();
        }
        else {


            final Dialog marketDialog = new Dialog(this, R.style.Theme_AppCompat_Light_Dialog);
            marketDialog.setContentView(R.layout.marker_dialog);
            Button closeButton = marketDialog.findViewById(R.id.closeButton);
            final TextView title = marketDialog.findViewById(R.id.title);
            final TextView heading=marketDialog.findViewById(R.id.heading);
            final TextView contact = marketDialog.findViewById(R.id.contact);
            final TextView general = marketDialog.findViewById(R.id.general);
            final TextView icu = marketDialog.findViewById(R.id.icu);
            final TextView avail = marketDialog.findViewById(R.id.avail);
            final ImageView callButton = marketDialog.findViewById(R.id.callButton);
            final ImageView tagImageView = marketDialog.findViewById(R.id.tagImageView);

            String s = marker.getSnippet();
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Stations").child(marker.getSnippet());
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        final Station station = dataSnapshot.getValue(Station.class);
                        heading.setText(station.title);
                        title.setText(station.name);
                        contact.setText("Contact : " + station.Cantact);
                        if (station.GeneralWardCapacity != null)
                            general.setText("General Ward Capacity : " + station.GeneralWardCapacity);
                        else general.setVisibility(View.GONE);
                        if (station.ICUCapacity != null)
                            icu.setText("ICU Capacity : " + station.ICUCapacity);
                        else icu.setVisibility(View.GONE);
                        if (station.Availability != null) {
                            if (station.Availability.equals("1")) {
                                avail.setBackgroundResource(R.color.green);
                                avail.setText("Available");
                            } else {
                                avail.setBackgroundResource(R.color.red);
                                avail.setText("Not Available");
                            }
                        } else avail.setVisibility(View.GONE);

                        if (station.title.equals("Hospital")) {
                            tagImageView.setBackgroundResource(R.drawable.hospital);
                        } else if (station.title.equals("Boat")) {
                            tagImageView.setBackgroundResource(R.drawable.ship);
                        } else if (station.title.equals("Fire Station")) {
                            tagImageView.setBackgroundResource(R.drawable.fire);
                        } else if (station.title.equals("Blood Bank")) {
                            tagImageView.setBackgroundResource(R.drawable.blood);
                        } else if (station.title.equals("Transport Vehicle")) {
                            tagImageView.setBackgroundResource(R.drawable.bus);
                        } else if (station.title.equals("Ambulance")) {
                            tagImageView.setBackgroundResource(R.drawable.ambulance);
                        }
                        else if (station.title.equals("Food")) {
                            tagImageView.setBackgroundResource(R.drawable.diet);
                        }

                        callButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(MapsActivity.this)
                                        .setCancelable(false)
                                        .setMessage("Do you want to make contact with " + station.name + " ?")
                                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Intent.ACTION_CALL);
                                                intent.setData(Uri.parse("tel:" + station.Cantact));
                                                startActivity(intent);
                                            }
                                        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                builder1.create().show();
                            }
                        });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    marketDialog.dismiss();
                }
            });

            marketDialog.show();
        }

        return true;
    }
}
