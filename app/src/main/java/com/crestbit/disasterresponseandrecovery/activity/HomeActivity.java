package com.crestbit.disasterresponseandrecovery.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crestbit.disasterresponseandrecovery.BuildConfig;
import com.crestbit.disasterresponseandrecovery.R;
import com.crestbit.disasterresponseandrecovery.api.RestApi;
import com.crestbit.disasterresponseandrecovery.api.WheatherRestApi;
import com.crestbit.disasterresponseandrecovery.model.Feed;
import com.crestbit.disasterresponseandrecovery.model.SensorData;
import com.crestbit.disasterresponseandrecovery.model.Wheather.AirAndPollen;
import com.crestbit.disasterresponseandrecovery.model.Wheather.DailyForecast;
import com.crestbit.disasterresponseandrecovery.model.Wheather.Day;
import com.crestbit.disasterresponseandrecovery.model.Wheather.WheatherData;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity {

    TextView rainProb,damLevel,rain,air,lowArea,damRelease,bridgeLevel,airQuality;
    public static String URL = "https://api.thingspeak.com/";
    public static String URL2 = "http://dataservice.accuweather.com/";
    private RestApi restApi;
    private WheatherRestApi wheatherRestApi;
    double perDamLevel=0.0;
    ImageView backButton,optionMenu,mapButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        backButton=findViewById(R.id.backButton);
        optionMenu=findViewById(R.id.optionMenu);
        mapButton=findViewById(R.id.mapButton);
        lowArea=findViewById(R.id.lowArea);
        damRelease=findViewById(R.id.damRelease);
        bridgeLevel=findViewById(R.id.bridgeLevel);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,MapsActivity.class));
            }
        });

        optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDamLevel();
            }
        });
        rain=findViewById(R.id.rain);
        air=findViewById(R.id.air);
        rainProb=findViewById(R.id.rainProb);
        damLevel=findViewById(R.id.damLevel);
        airQuality=findViewById(R.id.airQuality);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initRestApi();
        initWheatherApi();
       getDamLevel();


//getWheatherData();
    }

    private void getLowArea() {
        Observable<SensorData> observable= restApi.fechLowArea();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SensorData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","error");
                    }

                    @Override
                    public void onNext(SensorData user) {

                        List<Feed> feeds = user.getFeeds();
                        Feed feed = feeds.get(feeds.size() - 1);
                        Log.d("<<<<<", " "+feed.getField1());

                        Double lowValue= Double.valueOf(feed.getField1().trim());
                        if (lowValue>850)
                        {
                            lowArea.setText("No");
                        } else if (lowValue<850) {
                            lowArea.setText("Yes");
                        }
                        getDamRelease();
                    }

                });
    }

    private void getDamRelease() {
        Observable<SensorData> observable= restApi.fetchDamRelease();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SensorData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","error");
                    }

                    @Override
                    public void onNext(SensorData user) {

                        List<Feed> feeds = user.getFeeds();
                        Feed feed = feeds.get(feeds.size() - 1);
                        Log.d("<<<<<", " "+feed.getField1());
                        Double releaseValue= Double.valueOf(feed.getField1().trim());
                        if (releaseValue>850)
                        {
                            damRelease.setText("No");
                        } else if (releaseValue<850) {
                            damRelease.setText("Yes");
                        }

                        getBridgeLevel();

                    }

                });
    }

    private void getBridgeLevel() {
        Observable<SensorData> observable= restApi.fetchBridgeLevel();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SensorData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","error");
                    }

                    @Override
                    public void onNext(SensorData user) {

                        List<Feed> feeds = user.getFeeds();
                        Feed feed = feeds.get(feeds.size() - 1);
                        Log.d("<<<<<", " "+feed.getField1());
                        Double aDouble=Double.parseDouble(feed.getField1().trim());
                        if (aDouble<5)
                        bridgeLevel.setText("Alert");
                        else bridgeLevel.setText("Safe");

                    }

                });
    }


    private void getWheatherData() {
        Observable<WheatherData> observable= wheatherRestApi.fetchWheather();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WheatherData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","errorhiii");
                    }

                    @Override
                    public void onNext(WheatherData user) {

                        List<DailyForecast> dailyForecasts = user.getDailyForecasts();



                        int count=0;

                        DailyForecast dailyForecast=dailyForecasts.get(dailyForecasts.size()-1);
                        Day day1=dailyForecast.getDay();
                        rainProb.setText(String.valueOf(day1.getRainProbability())+"%");
                        List<AirAndPollen>airAndPollens=dailyForecast.getAirAndPollen();
                        AirAndPollen pollen=airAndPollens.get(0);

                        airQuality.setText(pollen.getCategory());
                        if (pollen.getCategoryValue()>4)
                        {
                            air.setText("Today's Air Quality "+pollen.getCategory());
                            air.setBackgroundResource(R.color.red);
                        }else if (pollen.getCategoryValue()>3)
                        {
                            air.setText("Today's Air Quality "+pollen.getCategory());
                            air.setBackgroundResource(R.color.yellow);
                        }
                        else {
                            air.setText("Today's Air Quality "+pollen.getCategory());
                            air.setBackgroundResource(R.color.green);
                        }

                        for (DailyForecast forecast:dailyForecasts)
                        {

                            Day day=forecast.getDay();

                            if (day.getRainProbability()>80&&day.getHoursOfRain()>=6&&day.getRain().getValue()>=6)
                            {
                                count=count+1;
                                if (count==2)
                                {
                                    if (perDamLevel>70)
                                    {
                                        rain.setText("Water will be release from dam on "+dailyForecast.getDate());
                                        rain.setBackgroundResource(R.color.red);
                                    }
                                }
                            }

                        }


                        Log.d("<<<<<", " "+day1.getRainProbability());

                    }

                });

    }

    private void getDamLevel() {
        Observable<SensorData> observable= restApi.fetchDamLevel();
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SensorData>() {
                    @Override
                    public void onCompleted() {
                        Log.d("<<<<<","oncompled");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("<<<<<","error");
                    }

                    @Override
                    public void onNext(SensorData user) {

                        List<Feed> feeds = user.getFeeds();
                        Feed feed = feeds.get(feeds.size() - 1);
                        Log.d("<<<<<", " "+feed.getField1());

                        Double level=11.58;
                        if (Double.parseDouble(feed.getField1().trim())>0) {
                            level = Double.parseDouble(feed.getField1().trim());
                        }


                        perDamLevel=100-((level/11.58)*100);
                        String s=String.format("%.2f",perDamLevel);
                        damLevel.setText(s+"%");
                        rain.setText("Current dam level is "+s+"% filled");
                        getWheatherData();
                        getLowArea();



                    }

                });
    }

    private void initRestApi() {
        OkHttpClient client = new OkHttpClient.Builder()/*.connectTimeout(100, TimeUnit.SECONDS).readTimeout(300, TimeUnit.SECONDS)*/.addInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE)).build();
        restApi =new Retrofit.Builder()
                .client(client)
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(getGsonConverter()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(RestApi.class);
    }

    private void initWheatherApi() {
        OkHttpClient client = new OkHttpClient.Builder()/*.connectTimeout(100, TimeUnit.SECONDS).readTimeout(300, TimeUnit.SECONDS)*/.addInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE)).build();
        wheatherRestApi =new Retrofit.Builder()
                .client(client)
                .baseUrl(URL2)
                .addConverterFactory(GsonConverterFactory.create(getGsonConverter()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(WheatherRestApi.class);
    }
    private Gson getGsonConverter() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }
}
