package com.crestbit.disasterresponseandrecovery.activity;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.crestbit.disasterresponseandrecovery.R;
import com.crestbit.disasterresponseandrecovery.model.Station;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.List;

public class AddServiceActivity extends AppCompatActivity {

    EditText name,contact,address;
    Spinner serviceSpinner;
    Button saveButton;
    ImageView backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        backButton=findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        name=findViewById(R.id.name);
        contact=findViewById(R.id.contactNumber);
        address=findViewById(R.id.address);
        serviceSpinner=findViewById(R.id.serviceSpinner);
        saveButton=findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().equals("")||address.getText().toString().equals("")||contact.getText().toString().equals(""))
                {
                    Toast.makeText(AddServiceActivity.this, "Empty fields are not allowed", Toast.LENGTH_SHORT).show();
                }
                else {
                    getLatLang(address.getText().toString().trim());
                }
            }
        });
    }

    private void getLatLang(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

            if (p1!=null)
            {
                DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Stations");
                String key=databaseReference.push().getKey();
                Station station=new Station();
                station.id=key;
                station.title=serviceSpinner.getSelectedItem().toString();
                station.lat= String.valueOf(location.getLatitude());
                station.lang= String.valueOf(location.getLongitude());
                station.name=name.getText().toString().trim();
                station.Cantact=contact.getText().toString().trim();
                databaseReference.child(key).setValue(station).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Toast.makeText(AddServiceActivity.this, "Service added successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
            else {
                Toast.makeText(this, "please enter proper address", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }

    }
}
